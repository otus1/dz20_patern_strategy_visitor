﻿using dz20_patern_strategy_visitor.main;
using dz20_patern_strategy_visitor.strategy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dz20_patern_strategy_visitor
{
    class Program
    {
        static void Main(string[] args)
        {
            Context context = new Context();

            Console.WriteLine("=== Point ===");
            Console.WriteLine(context.Request(new Request("Point", "Json", 1, 1)));
            Console.WriteLine(context.Request(new Request("Point", "Xml", 1, 1)));
            Console.WriteLine("");

            Console.WriteLine("=== Circle ===");
            Console.WriteLine(context.Request(new Request("Circle", "Json", 2, 2, 10)));
            Console.WriteLine(context.Request(new Request("Circle", "Xml",  2, 2, 10)));
            Console.WriteLine("");

            Console.WriteLine("=== Square ===");
            Console.WriteLine(context.Request(new Request("Square", "Json", 3)));
            Console.WriteLine(context.Request(new Request("Square", "Xml", 3)));

            Console.ReadKey();
        }
    }
}
