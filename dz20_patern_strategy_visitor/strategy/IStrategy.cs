﻿using dz20_patern_strategy_visitor.visitor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dz20_patern_strategy_visitor.strategy
{
    interface IStrategy
    {
        string Execute(IVisitor visitor, Request request);
    }
}
