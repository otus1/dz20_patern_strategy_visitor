﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dz20_patern_strategy_visitor.strategy
{
    public class Request
    {
        public readonly string visitor;
        public readonly string formatType;
        public readonly object obj;
        public readonly int x;
        public readonly int y;
        public readonly int r;
        public readonly int l;


        //point
        public Request(string visitor, string formatType, int x, int y)
        {
            this.visitor = visitor;
            this.formatType = formatType;
            this.x = x;
            this.y = y;
        }
        //circle
        public Request(string visitor, string formatType, int x, int y, int r)
            : this(visitor, formatType, x, y)
        {
            this.r = r;
        }

        //square
        public Request(string visitor, string formatType, int l)
            : this(visitor, formatType, 0, 0)
        {
            this.l = l;
        }
    }
}
