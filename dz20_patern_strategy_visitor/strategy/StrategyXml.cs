﻿using dz20_patern_strategy_visitor.visitor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dz20_patern_strategy_visitor.strategy
{
    public class StrategyXml: IStrategy
    {
        public string Execute(IVisitor visitor, Request request)
        {
            return visitor.Visit(this, request);
        }
    }
}
