﻿using dz20_patern_strategy_visitor.strategy;
using dz20_patern_strategy_visitor.visitor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dz20_patern_strategy_visitor.main
{
    class Context
    {
        IStrategy strategy;
        IVisitor visitor;


        public void SetStrategy(Request request)
        {
            var x = Activator.CreateInstance(null, "dz20_patern_strategy_visitor.strategy.Strategy" + request.formatType);
            strategy = (IStrategy)x.Unwrap();
        }

        public void SetVisitor(Request request)
        {
            var x = Activator.CreateInstance(null, "dz20_patern_strategy_visitor.visitor.Visitor" + request.visitor);
            visitor = (IVisitor)x.Unwrap();
        }

        public string Request(Request request)
        {
            SetStrategy(request);
            SetVisitor(request);
            return strategy.Execute(visitor, request);
        }
    }
}
