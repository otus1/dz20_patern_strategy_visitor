﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using dz20_patern_strategy_visitor.strategy;

namespace dz20_patern_strategy_visitor.visitor
{
    class VisitorCircle : IVisitor
    {
        public string Visit(StrategyJson strategy, Request request)
        {
            return $"{{\"circle\": {{x:{request.x}, y:{request.y}, r:{request.r} }} }}";
        }

        public string Visit(StrategyXml strategy, Request request)
        {
            return $"<circle> <x>{request.x}</x> <y>{request.y}</y> <r>{request.r}</r> </circle>";
        }
    }
}
