﻿using dz20_patern_strategy_visitor.strategy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dz20_patern_strategy_visitor.visitor
{
    public interface IVisitor
    {
        string Visit(StrategyJson strategy, Request request);
        string Visit(StrategyXml strategy, Request request);
    }
}
