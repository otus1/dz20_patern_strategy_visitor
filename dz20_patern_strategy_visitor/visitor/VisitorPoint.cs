﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using dz20_patern_strategy_visitor.strategy;

namespace dz20_patern_strategy_visitor.visitor
{
    class VisitorPoint : IVisitor
    {
        public string Visit(StrategyJson strategy, Request request)
        {
            return $"{{\"point\": {{x:{request.x}, y:{request.y}}} }}";
        }

        public string Visit(StrategyXml strategy, Request request)
        {
            return $"<point> <x>{request.x}</x> <y>{request.y}</y> </point>";
        }
    }
}
