﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using dz20_patern_strategy_visitor.strategy;

namespace dz20_patern_strategy_visitor.visitor
{
    class VisitorSquare : IVisitor
    {
        public string Visit(StrategyJson strategy, Request request)
        {
            return $"{{\"treangle\": {{l:{request.l}}} }}";
        }

        public string Visit(StrategyXml strategy, Request request)
        {
            return $"<square> <l>{request.l}</l> </square>";
        }
    }
}
